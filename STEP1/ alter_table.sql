/*------------------------------------
  テーブルの変更
-------------------------------------*/


/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;
drop table if exists syohinmst;

create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);


/*
  本題
--------------------------------------*/


/*
  フィールドを追加
  ALTER [IGNORE] TABLE テーブル名 ADD [COLUMN] フィールド名 データ型
         IGNORE -> フィールドが重複した場合のエラー回避

  最後に何も指定なし　-> 末尾に追加
  FIRST -> 先頭に追加
  AFTER フィールド名 -> 任意の場所に追加

*/


/* 末尾に追加 */
alter table syohinmst add haiban bool;

desc syohinmst;

/*
  フィールドの削除
  ALTER [IGNORE] TABLE テーブル名 DROP [COLUMN] フィールド名
         IGNORE -> フィールドが存在しない場合のエラー回避
*/

/* haibanを削除 */
alter table syohinmst drop haiban;

desc syohinmst;



/* shcdの後ろに追加 */
alter table syohinmst add syurui varchar(10) after shcd;

desc syohinmst;


/*
  フィールド名の変更
  ALTER [IGNORE] TABLE テーブル名 CHANGE [COLUMN] 元のフィールド名 変更するフィール名 データ型
         IGNORE -> フィールドが重複した場合のエラー回避
*/

alter table syohinmst change syurui bunrui varchar(10);


desc syohinmst;

/*
  後処理
--------------------------------------*/
drop table syohinmst;
drop database uriagekanri_shibata;
