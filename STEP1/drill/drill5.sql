/*------------------------------------
  練習5
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;

drop table if exists syohinmst;
create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  bunrui varchar(10),
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);

drop table if exists tokuisakimst;
create table tokuisakimst (
  tscd smallint(4) unsigned not null primary key,
  tsname varchar(30) not null,
  furigana varchar(30),
  yubin char(8),
  jyusyo1 varchar(30),
  jyusyo2 varchar(30),
  denwa varchar(30)
);

drop table if exists uriagedat;
create table uriagedat (
  uacd int(8) auto_increment primary key,
  hiduke date,
  tscd smallint(4),
  shcd smallint(4),
  suryo tinyint(3)
);

/*
  解答例
--------------------------------------*/

alter table syohinmst drop bunrui;

alter table tokuisakimst change tscd tscd smallint(5);
alter table uriagedat change tscd tscd smallint(5);

/*
  後処理
--------------------------------------*/
desc syohinmst;
desc tokuisakimst;
desc uriagedat;

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;