/*------------------------------------
  練習8
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;

drop table if exists uriagedat;
create table uriagedat (
  uacd int(8) auto_increment primary key,
  hiduke date,
  tscd smallint(5),
  shcd smallint(4),
  suryo tinyint(3)
);

load data infile '/Applications/MAMP/htdocs/noa/sql/txt/uriage.txt'
into table uriagedat;


/*
  解答例
--------------------------------------*/

insert into uriagedat set hiduke='2007-01-30', tscd=29000, shcd=1007, suryo=2;



/*
  後処理
--------------------------------------*/
select * from uriagedat;

drop table uriagedat;
drop database uriagekanri_shibata;