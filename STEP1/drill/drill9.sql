/*------------------------------------
  練習9
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;
drop table if exists syohinmst;

create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);

insert into syohinmst values (
  1001,
  'デスクトップパソコン',
  98000
), (
  1002,
  'タワー型パソコン',
  107000
), (
  1003,
  'ノートパソコン',
  124000
), (
  1004,
  'インクジェットプリンタ',
  18500
), (
  1005,
  'レーザープリンタ',
  18500
), (
  1006,
  'スキャナ',
  18500
), (
  1007,
  'デジタルカメラ',
  18500
), (
  1008,
  'CD-RDドライブ',
  18500
), (
  1009,
  'LANカード',
  18500
), (
  1010,
  'USBメモリ',
  3500
);


/*
  解答例
--------------------------------------*/


update syohinmst set tanka=136000 WHERE shcd=1005;


/*
  後処理
--------------------------------------*/

select * from syohinmst;

drop table syohinmst;
drop database uriagekanri_shibata;