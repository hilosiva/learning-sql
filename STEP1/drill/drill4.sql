/*------------------------------------
  練習3
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;


drop table if exists tokuisakimst;
drop table if exists uriagedat;

/*
  解答例
--------------------------------------*/


create table tokuisakimst (
  tscd smallint(4) unsigned not null primary key,
  tsname varchar(30) not null,
  furigana varchar(30),
  yubin char(8),
  jyusyo1 varchar(30),
  jyusyo2 varchar(30),
  denwa varchar(30)
);


create table uriagedat (
  uacd int(8) auto_increment primary key,
  hiduke date,
  tscd smallint(4),
  shcd smallint(4),
  suryo tinyint(3)
);

/*
  後処理
--------------------------------------*/
desc tokuisakimst;
desc uriagedat;


drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;