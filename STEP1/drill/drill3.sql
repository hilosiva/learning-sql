/*------------------------------------
  練習3
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;

drop table if exists syohinmst;

/*
  解答例
--------------------------------------*/

create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);


/*
  後処理
--------------------------------------*/
desc syohinmst;

drop table syohinmst;
drop database uriagekanri_shibata;


