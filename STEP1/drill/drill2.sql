/*------------------------------------
  練習2
-------------------------------------*/

/*
  解答例
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
create database zaikokanri_shibata default character set utf8;

show databases;


/*
  後処理
--------------------------------------*/
drop database uriagekanri_shibata;
drop database zaikokanri_shibata;