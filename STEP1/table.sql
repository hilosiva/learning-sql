/*------------------------------------
  テーブルの操作
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;
use uriagekanri_shibata;



/*
  本題
--------------------------------------*/

/*
  テーブル一覧の表示
  SHOW TABLES;
*/
show tables;

/*
  テーブルの作成
  CREATE TABLE テーブル名 ( フィールド名1, フィールド名2, );
*/



/*
  テーブル作成前に削除する
  DROP TABLE [ IF EXISTS ] テーブル名;

    IF EXISTS　--> 削除すべきテーブルが存在しない場合でも、エラーにしない
*/
drop table if exists syohinmst;


/*
  テーブルを作成する
  CREATE TABLE テーブル名(  フィールド1, フィールド2, …  );

  1.クエリは改行しても良い
  2.クエリはインデントを付けても良い
*/


create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);
/*
  データ型
    INT -> 整数のみ格納可能
    DOUBLE -> 浮動小数点数
    CHAR ->　固定長文字列
    VARCHAR -> 可変長文字列
    DATE -> 日付
    DATETIME -> 日時
    ENUM -> 列挙

      UNSIGNED ・・・ 正の数しか格納できなくする
*/



/*
  フィールド定義
  制約

  NOT NULL -> 入力必須（空白を許可しない）
  DEFAULT -> デフォルト値
  AUTO_INCREMENT -> 新規レコードの挿入時に自動的に値を設定する
                    ※設定される値は１から始まる一意の連番となる

  インデックス（検索が早くなる）
    PRIMARY KEY -> 指定されたフィールドを主キーとする
                   「主キー」 ・・・ テーブルに１つだけ存在し、レコードが一意に特定できる事を保障する
                   ※PRYMARY KEY 制約には自動的に UNIQUE , NOT NULL の制約が付加される
    KEY -> キー
    UNIQUE -> ユニークキー（同じ値の挿入を許可しない）

    !! PRIMARY KEY は AUTO_INCREMENT と 組み合わせて使う !!
*/




/*
  フィールドの表示
  DESC テーブル名;
*/

desc syohinmst;


/*
  テーブルの削除
  DROP TABLE [ IF EXISTS ] テーブル名;

    - IF EXISTS ・・・　テーブルが存在しない場合でもエラーにしない。
*/

drop table syohinmst;


/*
  後処理
--------------------------------------*/
drop database uriagekanri_shibata;
