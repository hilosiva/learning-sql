/*------------------------------------
  データベースの操作
-------------------------------------*/

/*
  データベースの一覧を表示
  show databases;
*/
show databases;

/*
  データベースの作成
  CREATE DATABASE データベース名 [DEFAULT CHARACTER SET UTF8];

*/
create database uriagekanri_shibata default character set utf8;

/*
  データベースに接続
  USE データベース名;
*/
use uriagekanri_shibata;

/*
  データベースの削除
  DROP DATABASE データベース名
*/

drop database uriagekanri_shibata;


