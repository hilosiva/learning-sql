/*------------------------------------
  レコードの挿入
-------------------------------------*/


/*
  事前準備
--------------------------------------*/
create database uriagekanri_shibata default character set utf8;

use uriagekanri_shibata;
drop table if exists syohinmst;
create table syohinmst (
  shcd smallint(4) unsigned not null primary key,
  shname varchar(255) not null,
  tanka int(6) unsigned not null
);

drop table if exists tokuisakimst;
create table tokuisakimst (
  tscd smallint(5) unsigned not null primary key,
  tsname varchar(30) not null,
  furigana varchar(30),
  yubin char(8),
  jyusyo1 varchar(30),
  jyusyo2 varchar(30),
  denwa varchar(30)
);

drop table if exists uriagedat;
create table uriagedat (
  uacd int(8) auto_increment primary key,
  hiduke date,
  tscd smallint(5),
  shcd smallint(4),
  suryo tinyint(3)
);

/*
  本題
--------------------------------------*/

/*
  テーブルに対してレコードを挿入する
  INSERT INTO テーブル名(フィールド名1, フィールド名2, ...) VALUES(挿入するデータ1, 挿入するデータ2, ... );

  *文字列型のフィールドにデータを挿入する場合はシングルクォーテーションで囲む
*/
insert syohinmst values(1001, 'デスクトップパソコン', 98000);

select * from syohinmst;



/*
  外部ファイルのデータを読み込み
  LOAD DATA INFILE 'フルパス' INTO TABLE テーブル名
*/

load data infile '/Applications/MAMP/htdocs/noa/sql/txt/tokuisaki.txt' into table tokuisakimst;

select * from tokuisakimst;




/* 下準備 */
insert into syohinmst values(9999, 'iPhone5', 50000 );
select * from syohinmst;


/*
　置き換え
  REPLACE INTO テーブル名(フィールド名1, フィールド名2, ...) VALUES(挿入するデータ1, 挿入するデータ2, ... );
*/

replace into syohinmst(shcd, shname ,tanka) values(9999, 'iPhone6', 80000);
select * from syohinmst;

/*
  更新
  UPDATE テーブル名 SET フィールド名=値 WHERE 条件式;
*/
UPDATE syohinmst SET tanka=70000 WHERE shcd=9999;
select * from syohinmst;


/*
  削除
  DELETE FROM テーブル名 WHERE 条件式;
*/
DELETE FROM syohinmst WHERE shcd=9999;
select * from syohinmst;


/*
  後処理
--------------------------------------*/
drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;
