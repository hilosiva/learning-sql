-- テーブルの変更

-- フィールドの追加
ALTER TABLE syohinmst ADD haiban bool;
DESC syohinmst;

-- フィールドの削除
ALTER TABLE syohinmst DROP haiban;
DESC syohinmst;

-- 指定した位置にフォールドを追加
ALTER TABLE syohinmst ADD syurui VARCHAR(10) AFTER shcd;
DESC syohinmst;

-- フィールドの変更
ALTER TABLE syohinmst CHANGE syurui bunrui VARCHAR(10);
DESC syohinmst;

-- 練習5
ALTER TABLE syohinmst DROP bunrui;
DESC syohinmst;

ALTER TABLE tokuisakimst MODIFY tscd SMALLINT(5) UNSIGNED;
DESC tokuisakimst;

ALTER TABLE uriagedat MODIFY tscd SMALLINT(5) UNSIGNED;
DESC uriagedat;
