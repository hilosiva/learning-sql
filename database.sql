-- データーベースの操作

-- データーベースの作成
CREATE DATABASE uriagekanri_shibata
 DEFAULT CHARACTER SET UTF8;

 -- データーベースの一覧表示
SHOW DATABASES;

-- データーベースの削除
DROP DATABASE uriagekanri_shibata;

-- データーベースの選択
USE uriagekanri_shibata;

-- 練習2
CREATE DATABASE uriagekanri_shibata
DEFAULT CHARACTER SET UTF8;

CREATE DATABASE zaikokanri_shibata
DEFAULT CHARACTER SET UTF8;

SHOW DATABASES;
