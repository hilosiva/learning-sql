-- レコードの抽出

-- 基本形
SELECT shname, tanka FROM syohinmst;

-- 練習10
-- 1
SELECT tsname, yubin, jusyo1, jusyo2 FROM tokuisakimst;

-- 2
SELECT * FROM uriagedat;

-- 条件をつける
SELECT * FROM syohinmst
  WHERE tanka >= 50000; -- 比較するフィールド名 比較演算子 比較する値

-- 練習11
-- 1
SELECT tsname, yubin, jusyo1, jusyo2
  FROM tokuisakimst
  WHERE jusyo1 != '東京都';

-- 2
SELECT * FROM uriagedat
  WHERE suryo < 10;

-- 3
SELECT * FROM uriagedat
  WHERE hiduke >= '2007-01-01';

-- 論理演算子
SELECT * FROM syohinmst
  WHERE tanka >= 100000 OR tanka < 10000;

-- 練習12

-- 1
SELECT tsname, yubin, jusyo1, jusyo2
  FROM tokuisakimst
  WHERE jusyo1 = '神奈川県' OR jusyo1 = '埼玉県';

-- 2
SELECT * FROM uriagedat
  WHERE hiduke >= '2006-12-01' AND hiduke < '2007-01-01';

-- 3
SELECT * FROM uriagedat
  WHERE (shcd = 1001 OR shcd = 1002) AND hiduke >= '2007-01-01';

-- その他の演算子

-- 範囲検索（BETWEEN）
SELECT * FROM syohinmst
  WHERE tanka BETWEEN 50000 AND 100000;

-- いずれか検索（IN）
SELECT * FROM uriagedat
  WHERE suryo IN(5, 8, 10);

-- あいまい検索（LIKE）
SELECT * FROM syohinmst
  WHERE shname LIKE '%プリンタ%';


-- 練習13
-- 1
SELECT tsname, yubin, jusyo1, jusyo2
  FROM tokuisakimst
  WHERE jusyo1 IN('大阪府', '京都府', '兵庫県');

-- 2
SELECT * FROM uriagedat
  WHERE hiduke BETWEEN '2006-11-01' AND '2006-11-30';

-- 3
SELECT * FROM syohinmst
  WHERE shname LIKE '%パソコン%';

-- レコード数の制限
-- uriagedat のレコードを先頭から10件抽出
SELECT * FROM uriagedat
  LIMIT 10;

-- uriagedat のレコードを11件目（uacd: 11）から10件抽出
SELECT * FROM uriagedat
  LIMIT 10, 10;

-- レコードの並び替え
SELECT * FROM syohinmst
  ORDER BY tanka DESC;


-- 練習14
-- 1
SELECT * FROM uriagedat
  ORDER BY hiduke ASC;

-- 2
SELECT * FROM uriagedat
  ORDER BY hiduke DESC, suryo DESC;

-- 集約関数
-- 平均値
SELECT AVG(tanka) FROM syohinmst;

-- 商品（shcd）ごとに、suryo の合計値
SELECT shcd, SUM(suryo) FROM uriagedat
  GROUP BY shcd;

  -- フィールドに別名をつける
SELECT shcd, SUM(suryo) AS 合計 FROM uriagedat
  GROUP BY shcd;

-- 集約関数の結果に条件をつける
SELECT shcd, SUM(suryo) AS 合計 FROM uriagedat
  GROUP BY shcd
  HAVING 合計 >= 300;

-- テーブルの結合（内部結合）
SELECT
    uriagedat.uacd,
    uriagedat.hiduke,
    uriagedat.tscd,
    uriagedat.shcd,
    syohinmst.shname,
    uriagedat.suryo
  FROM
    uriagedat JOIN syohinmst
  ON
    uriagedat.shcd = syohinmst.shcd;

-- 練習16
SELECT
    uriagedat.uacd,
    uriagedat.hiduke,
    uriagedat.tscd,
    tokuisakimst.tsname,
    uriagedat.shcd,
    syohinmst.shname,
    uriagedat.suryo
  FROM
    uriagedat JOIN
    syohinmst JOIN
    tokuisakimst
  ON
    uriagedat.shcd = syohinmst.shcd
    AND
    uriagedat.tscd = tokuisakimst.tscd;

-- テーブル名に別名をつける（相関名）
SELECT
    u.uacd,
    u.hiduke,
    u.tscd,
    t.tsname,
    u.shcd,
    s.shname,
    u.suryo
  FROM
    uriagedat AS u JOIN
    syohinmst AS s JOIN
    tokuisakimst AS t
  ON
    u.shcd = s.shcd
    AND
    u.tscd = t.tscd;


-- 練習問題
-- 上記で結合したテーブルに以下の条件を付けて抽出してみて下さい
-- 2006年11月のデータを 数量 が多い順に 5件 抽出
-- なお、表示するフィールドは、
-- 「uacd」、「hiduke」、「tsname」、「shname」、「tanka」、「suryo」
SELECT
    u.uacd,
    u.hiduke,
    t.tsname,
    s.shname,
    s.tanka,
    u.suryo
  FROM
    uriagedat AS u JOIN
    syohinmst AS s JOIN
    tokuisakimst AS t
  ON
    u.shcd = s.shcd
    AND
    u.tscd = t.tscd
  WHERE
    u.hiduke >= '2006-11-01'
    AND
    u.hiduke < '2006-12-01'
  ORDER BY
    u.suryo DESC
  LIMIT
    5;
