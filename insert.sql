-- レコードの挿入
INSERT INTO syohinmst(shcd, shname, tanka)
  VALUES(1001, "デスクトップパソコン", 98000);

-- レコードの一覧表示
SELECT * FROM syohinmst;


-- 練習6
INSERT INTO syohinmst VALUES(1002, "タワー型パソコン", 107000);
INSERT INTO syohinmst VALUES(1003, "ノートパソコン", 124000);
INSERT INTO syohinmst VALUES(1004, "インクジェットプリンタ", 18500);
INSERT INTO syohinmst VALUES(1005, "レーザープリンタ", 158000);
INSERT INTO syohinmst VALUES(1006, "スキャナ", 28000);
INSERT INTO syohinmst VALUES(1007, "デジタルカメラ", 97000);
INSERT INTO syohinmst VALUES(1008, "CD-RWドライブ", 39000);
INSERT INTO syohinmst VALUES(1009, "LANカード", 9800);
INSERT INTO syohinmst VALUES(1010, "USBメモリ", 3500);
SELECT * FROM syohinmst;

-- ファイルの読み込み
-- LOAD DATA INFILE 'ファイルパス' INTO TABLE tokuisakimst;
-- LOAD DATA INFILE 'ファイルパス' INTO TABLE uriagedat;

SELECT * FROM tokuisakimst;
SELECT * FROM uriagedat;

-- 練習9
UPDATE syohinmst
  SET tanka=136000
  WHERE shcd=1005;

SELECT * FROM syohinmst;

-- レコードの削除
INSERT INTO syohinmst VALUES(1011, "iPad Pro", 100000);
SELECT * FROM syohinmst;

DELETE FROM syohinmst
  WHERE shcd=1011;
