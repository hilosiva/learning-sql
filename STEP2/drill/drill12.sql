/*------------------------------------
  練習12
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

/*
  解答例
--------------------------------------*/

/* 問1 */
select tsname, yubin, jusyo1, jusyo2 from tokuisakimst where jusyo1 = '神奈川県' or jusyo1 = '埼玉県';

/* 問2 */
select * from uriagedat where hiduke >= '2006-12-01' and hiduke <= '2006-12-31';

/* 問3 */
select * from uriagedat where (shcd = 1001 or shcd = 1002) and hiduke >= '2007-01-01';


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;