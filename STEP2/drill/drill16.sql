/*------------------------------------
  練習16
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

insert into tokuisakimst(
  tscd, tsname, furigana, jusyo1, jusyo2
) values(
  33010, 'ノア商事', 'ﾉｱｼｮｳｼﾞ', '大阪府', '北区万歳町1-1-1'
);



/*
  解答例
--------------------------------------*/

/* 問1 */
select uacd, hiduke, uriagedat.tscd,tsname, uriagedat.shcd, shname, suryo
from uriagedat, syohinmst, tokuisakimst
where uriagedat.shcd = syohinmst.shcd and uriagedat.tscd = tokuisakimst.tscd;
/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;