/*------------------------------------
  練習14
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

insert into tokuisakimst(
  tscd, tsname, furigana, jusyo1, jusyo2
) values(
  33010, 'ノア商事', 'ﾉｱｼｮｳｼﾞ', '大阪府', '北区万歳町1-1-1'
);



/*
  解答例
--------------------------------------*/

/* 問1 */
select * from uriagedat order by hiduke asc;

/* 問2 */
select * from uriagedat order by hiduke asc, suryo desc;

/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;