/*------------------------------------
  練習18
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

insert into tokuisakimst(
  tscd, tsname, furigana, jusyo1, jusyo2
) values(
  33010, 'ノア商事', 'ﾉｱｼｮｳｼﾞ', '大阪府', '北区万歳町1-1-1'
);



/*
  解答例
--------------------------------------*/


/* 1 */
select uacd, hiduke, X.tscd, tsname, X.shcd, shname, tanka, suryo, tanka * suryo as Kingaku
from uriagedat as X, tokuisakimst as Y, syohinmst as Z
where X.tscd = Y.tscd and X.shcd = Z.shcd;


/* 2 */
select hiduke, count(*) as Kensu from uriagedat group by hiduke order by Kensu desc;


/* 3 */
select X.shcd, shname, AVG(suryo) as heikin
from uriagedat as X, syohinmst as Y
where X.shcd = Y.shcd
group by X.shcd;


/* 4 */
select uacd, hiduke, X.tscd, tsname, X.shcd, shname, tanka, suryo, tanka * suryo as Kingaku
from uriagedat as X, tokuisakimst as Y, syohinmst as Z
where X.tscd = Y.tscd and X.shcd = Z.shcd
and hiduke = (select MAX(hiduke) from uriagedat);

/* 5 */
select uacd, hiduke, X.tscd, tsname, X.shcd, shname, tanka, suryo, tanka * suryo as Kingaku
from uriagedat as X, tokuisakimst as Y, syohinmst as Z
where X.tscd = Y.tscd and X.shcd = Z.shcd
and hiduke between '2007-01-01' and '2007-01-31';

/* 6 */
select uacd, hiduke, X.tscd, tsname, X.shcd, shname, tanka, suryo, tanka * suryo as Kingaku
from uriagedat as X, tokuisakimst as Y, syohinmst as Z
where X.tscd = Y.tscd and X.shcd = Z.shcd
and hiduke = '2006-11-04'
and shname like '%プリンタ%';


/* 7 */
select uriagedat.tscd, tsname, sum(tanka * suryo) as gokeikinkgaku
from uriagedat, tokuisakimst, syohinmst
having gokeikinkgaku >= 10000000
order by gokeikinkgaku desc;

/* 8 */
select shname from syohinmst
where shcd in (select shcd from uriagedat where hiduke = '2007-01-09');

/* 9 */
select hiduke, X.shcd, shname, X.tscd, tsname, suryo
from uriagedat as X, syohinmst as Y, tokuisakimst as Z
where X.shcd = Y.shcd AND X.tscd = Z.tscd
and hiduke in (select max(hiduke) from uriagedat);

/* 10 */
select hiduke, X.shcd, shname, X.tscd, tsname, suryo, tanka * suryo as kingaku
from uriagedat as X, syohinmst as Y, tokuisakimst as Z
where X.shcd = Y.shcd and X.tscd = Z.tscd
having kingaku >= (select avg(suryo * tanka) from uriagedat, syohinmst
where uriagedat.shcd = syohinmst.shcd);


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;