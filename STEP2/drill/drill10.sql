/*------------------------------------
  練習10
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

/*
  解答例
--------------------------------------*/

/* 問1 */
select tsname, yubin, jusyo1, jusyo2 from tokuisakimst;

/* 問2 */
select * from uriagedat;


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;