/*------------------------------------
  練習15
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

insert into tokuisakimst(
  tscd, tsname, furigana, jusyo1, jusyo2
) values(
  33010, 'ノア商事', 'ﾉｱｼｮｳｼﾞ', '大阪府', '北区万歳町1-1-1'
);



/*
  解答例
--------------------------------------*/

/* 問1 */
select count(*) as tokuisakikensu from tokuisakimst;

/* 問2 */
select count(*) as syohinkensu from syohinmst;

/* 問3 */
select tscd, sum(suryo) as gokei from uriagedat group by tscd;

/* 問4 */
select tscd, sum(suryo) as gokei from uriagedat group by tscd having gokei < 50;

/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;