/*------------------------------------
  練習13
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

/*
  解答例
--------------------------------------*/

/* 問1 */
select tsname, yubin, jusyo1, jusyo2 from tokuisakimst where jusyo1 in('大阪府', '京都府', '兵庫県');

/* 問2 */
select * from uriagedat where hiduke between '2006-11-01' and '2006-11-31';

/* 問3 */
select * from syohinmst where shname like '%パソコン%';


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;