/*------------------------------------
  練習11
-------------------------------------*/

/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行

/*
  解答例
--------------------------------------*/

/* 問1 */
select tsname, yubin, jusyo1, jusyo2 from tokuisakimst where jusyo1 != '東京都';

/* 問2 */
select * from uriagedat where suryo < 10;

/* 問3 */
select * from uriagedat where hiduke >= '2007-01-01';


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;