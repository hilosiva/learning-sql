/*------------------------------------
  レコードの抽出（SELECT文）
-------------------------------------*/


/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行



/*
  本題
--------------------------------------*/

/*
  ユーザーの権限と管理

  ユ−ザーの新規作成
  GRANT 権限 ON データベース名.* TO ユーザー名 IDENTIFIED BY 'パスワード';
*/

grant all on uriagekanri.* to shibata@localhost identified by 'abcd';

use mysql;
select host, user, password from user;


/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;