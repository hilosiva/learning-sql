/*------------------------------------
  レコードの抽出（SELECT文）
-------------------------------------*/


/*
  事前準備
--------------------------------------*/
-- step1.sqlの実行



/*
  本題
--------------------------------------*/

/*
  基本的なレコードの抽出
  SELECT フィールド名 FROM テーブル名;

    フィールド名
      - 複数のフィールドを検索対象にする：フィールド名を「,（半角カンマ）」で区切る
      - 全てのフィールドを検索対象にする：「*（アスタリスク）」を使う
*/

select shname, tanka from syohinmst;


/*
  条件を指定してレコードの抽出
  SELECT フィールド名 FROM テーブル名 WHERE 条件式;
*/

/*
  比較演算子
    A = B : AとBは等しい
    A > B : AはBより大きい
    A < B : AはBより小さい
    A >= B : AはB以上
    A <= B : AはB以下
    A != B : AとBは等しくない
*/

select * from syohinmst where tanka >= 50000;



/*
  論理演算子
    A AND B : AかつB
    A OR B : AまたはB
    NOT A : Aではない
*/

select * from syohinmst where tanka >= 100000 or tanka < 10000;

/*
  その他の演算子
*/

/*
  範囲検索
  BETWEEN A AND B : AからBの間
*/
select * from syohinmst where tanka between 50000 and 100000;

/*
  いずれか検索
  IN(値1, 値2, 値3) : いずれかを含む
*/
select * from uriagedat where suryo in(5, 8, 10);

/*
  あいまい検索
  LIKE  パターン : パターンと一致する

    - % : 0文字以上の文字列
    - _ : 1文字
*/
select * from syohinmst where shname like '%プリンタ%';

select * from syohinmst where shname like '____プリンタ';


/*
  空白の検索
  IS NULL : 空白のデータ
*/

/* 準備 */
insert into tokuisakimst(
  tscd, tsname, furigana, jusyo1, jusyo2
) values(
  33010, 'ノア商事', 'ﾉｱｼｮｳｼﾞ', '大阪府', '北区万歳町1-1-1'
);


select * from tokuisakimst where yubin is null;

/*
  条件の並び替え
  SELECT フィールド名 FROM テーブル名 ORDER BY フィールド名 並び替え

    並び替え
      - ASC : 昇順
      - DESC : 降順
*/

select * from syohinmst order by tanka desc;


/*
  集約関数
  SELECT 関数名(フィールド名) FROM テーブル名


  AVG - 平均
  SUM - 合計
  MAX - 最大値
  MIN - 最小値
  COUNT - レコード数

*/
select avg(tanka) from syohinmst;

/*
  グループごとに集約
  SELECT 関数名(フィールド名) FROM テーブル名 GROUP BY フィールド名
*/

select shcd, sum(suryo) from uriagedat group by shcd;

/*
  集約したフィール名
  SELECT 関数名(フィールド名) AS 新フィールド名 FROM テーブル名
*/
select shcd, sum(suryo) as gokei from uriagedat group by shcd;

/*
  集約した結果に条件をつける
  SELECT 関数名(フィールド名) AS 新フィールド名 FROM テーブル名
*/
select shcd, sum(suryo) as gokei from uriagedat group by shcd having gokei >= 300;


/*
  テーブルの結合
  SELECT テーブル名A.フィールド名
  FROM テーブル名A, テーブル名B
  WHERE テーブル名A.フィールド名 = テーブル名B.フィールド名;
*/

select uacd, hiduke tscd, uriagedat.shcd, shname, suryo
from uriagedat, syohinmst
where uriagedat.shcd = syohinmst.shcd;

/*
  相関名
  SELECT 相関名A.フィールド名
  FROM テーブル名 AS 相関名A, テーブル名 AS　相関名B
  WHERE 相関名A.フィールド名 = 相関名B.フィールド名;
*/

select uacd, hiduke, tscd, X.shcd, shname, suryo
from uriagedat as X, syohinmst as Y
where X.shcd = Y.shcd;


/*
  算術演算子を使う
*/
select uacd, hiduke, tscd, X.shcd, suryo, tanka, tanka * suryo as Kingaku
from uriagedat as X, syohinmst as Y
where X.shcd = Y.shcd;


/*
  サブクエリ
*/

/*
  select avg(suryo) from uriagedat;

  select shname from uriagedat, syohinmst
  where uriagedat.shcd = syohinmst.shcd and suryo < 25.61 group by shname;

  上記は下記と同様
*/

select shname from uriagedat, syohinmst
where uriagedat.shcd = syohinmst.shcd and
suryo < (select avg(suryo) from uriagedat)
group by shname;




/*
  INを使ったサブクエリ
*/

/*
  select tscd from uriagedat where hiduke = '2006-12-01';

  select tsname, denwa from tokuisakimst
  where tscd in (10000, 10020, 18000, 20000, 15010);

  上記は下記と同様
*/

select tsname, denwa from tokuisakimst
where tscd in (select tscd from uriagedat where hiduke = '2006-12-01');


/*
  EXISTSを使ったパターン
*/

select tsname, denwa from tokuisakimst
where exists (select * from uriagedat
where hiduke = '2006-12-01' and uriagedat.tscd = tokuisakimst.tscd);

/*
  後処理
--------------------------------------*/

drop table syohinmst;
drop table tokuisakimst;
drop table uriagedat;

drop database uriagekanri_shibata;